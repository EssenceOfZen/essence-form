# Essence FORM

EoZ's styling/animation for web related applications

**Version:** *0.3.5*

## Goals:

#### WORK IN PROGRESS:

1. Popup Menu + Animation
2. More style options
3. Fine-tuning the UI related elements
4. Create Table styling.

#### PLANNED:

* Variety of element animations
* Footer (auto-adjusting based on screen size and content on screen.)
* User account menu/panel functionality + styling

##### COMPLETED:
* Medium Screen size tweaking
* Small Screen size tweaking

## Change Log:

#### Version 0.3.0:

- Updated Button classes and structure
- Added "grow" animation class
- Removed default border around the "panel" class
- Added "border" class for panels

#### Version 0.2.0:

- Changed NavBar positioning to not rely on JavaScript, but rather CSS.
- Displaying Examples of different grid sizes for elements using the large, medium, and small classes.
- Footer added.
- Color classes added for CSS.

#### Version 0.1.0:

- Example page, CSS, JS:
 - Adding usable styles for buttons
 - Grid System for full size screens done
 - Navigation panels
 - "Planes" and their styling: (Hightened Plane, Base Plane, Ethereal Plane)
 - Animation of Nav/Tool bar's shadow depending on location of screen.
 - Panel styling + "content" spacing.


#### Version 0.0.1:

- Initial release:
 - Basic foundation for the project.
